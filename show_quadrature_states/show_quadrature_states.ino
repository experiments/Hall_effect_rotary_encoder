/*
 * Show quadrature states of two Hall effect sensors.
 *
 * Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
 * SPDX-License-Identitier: WTFPL
 */

#define HALL_CHANNEL_A 2
#define HALL_CHANNEL_B 3
#define LED_PIN 13

int hall_state_A = 0;
int hall_state_B = 0;

int old_hall_state_A = -1;
int old_hall_state_B = -1;

void setup()
{
	pinMode(HALL_CHANNEL_A, INPUT);
	pinMode(HALL_CHANNEL_B, INPUT);
	pinMode(LED_PIN, OUTPUT);
	Serial.begin(115200);
}

void loop()
{
	hall_state_A = digitalRead(HALL_CHANNEL_A);
	hall_state_B = digitalRead(HALL_CHANNEL_B);

	if (hall_state_A != old_hall_state_A ||
	    hall_state_B != old_hall_state_B)
	{
		digitalWrite(LED_PIN, hall_state_A);

		old_hall_state_A = hall_state_A;
		old_hall_state_B = hall_state_B;

		Serial.print(hall_state_A);
		Serial.print(" ");
		Serial.print(hall_state_B);
		Serial.print("\r\n");
	}
}
