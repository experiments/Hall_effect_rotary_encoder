/*
 * Rotary encoder example
 *
 * Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
 * SPDX-License-Identitier: WTFPL
 */

#include <DFRkeypad.h>
#include <Encoder.h>
#include <LiquidCrystal.h>

#define ENCODER_CHANNEL_A 2
#define ENCODER_CHANNEL_B 3

#define COUNTS_PER_ROTATION 84

#define LCD_RS 8
#define LCD_EN 9
#define LCD_D4 4
#define LCD_D5 5
#define LCD_D6 6
#define LCD_D7 7
#define LCD_BL 10

#define BACKLIGHT_VALUE 1023

long old_counts = 0;
Encoder encoder(ENCODER_CHANNEL_A, ENCODER_CHANNEL_B);
LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);

void setup() {
	pinMode(LCD_BL, OUTPUT);
	analogWrite(LCD_BL, BACKLIGHT_VALUE);

	lcd.begin(16, 2);

	lcd.setCursor(1, 0);
	lcd.print("Rotary Encoder");
	lcd.setCursor(1, 1);
	lcd.print("https://ao2.it");

	DFRkeypad::FastADC(true);
	DFRkeypad::iDEFAULT_THRESHOLD=140;
}

void loop() {
	byte key = DFRkeypad::GetKey();
	if (key == DFRkeypad::eSELECT) {
		encoder.write(0);
	}

	long new_counts = encoder.read();
	if (new_counts != old_counts) {
		old_counts = new_counts;

		lcd.clear();

		lcd.setCursor(0, 0);
		lcd.print("Count: ");
		lcd.setCursor(8, 0);
		lcd.print(new_counts);

		/* Position normalized between 0 and COUNTS_PER_ROTATION */
		long position = ((new_counts % COUNTS_PER_ROTATION) + COUNTS_PER_ROTATION) % COUNTS_PER_ROTATION;

		float angle = position * (360.0 / COUNTS_PER_ROTATION);

		lcd.setCursor(0, 1);
		lcd.print("Angle: ");
		lcd.setCursor(8, 1);
		lcd.print(angle);
	}

	delay(20);
}
